import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
// Application modules
import {AuthModule} from './pages/auth/auth.module';
import {NavComponent} from './layout/nav/nav.component';
import {HttpErrorHandler} from './shared/_services/http-handle-error.service';

import {AppHttpInterceptorService} from './shared/_services/app-http-interceptor.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ServersModule} from "./pages/servers/servers.module";

@NgModule({
    declarations: [
        AppComponent,
        NavComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ServersModule,
        AuthModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
        NgbModule.forRoot(),
        FormsModule
    ],
    providers: [
        Title,
        HttpErrorHandler,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptorService,
            multi: true
        }
    ],

    bootstrap: [AppComponent],
})
export class AppModule {
}
