
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/_guards/auth.guard';

import { ServerListComponent } from './server-list/server-list.component';

const routes: Routes = [
  {
    path: 'servers',
    children: [
      {
        path: '',
        component: ServerListComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServersRoutingModule { }
