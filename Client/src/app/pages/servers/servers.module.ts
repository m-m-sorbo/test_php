import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServerListComponent} from './server-list/server-list.component';
import {HttpClientModule} from '@angular/common/http';
import {ServersRoutingModule} from "./servers-routing.module";
import {ServersComponent} from "./servers.component";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        ServersRoutingModule,
        HttpClientModule,
        NgbModule,
        FormsModule,
    ],
    declarations: [ServersComponent, ServerListComponent]
})
export class ServersModule {
}
