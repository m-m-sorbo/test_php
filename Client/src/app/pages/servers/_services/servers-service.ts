import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
// App import
import {environment} from '../../../../environments/environment';
import {Server} from '../server';
import {HandleError, HttpErrorHandler} from '../../../shared/_services/http-handle-error.service';

@Injectable({
    providedIn: 'root'
})
export class ServersService {
    private readonly apiUrl = environment.apiUrl;
    private serversUrl = this.apiUrl + '/servers';
    private handleError: HandleError;

    constructor(private http: HttpClient,
                httpErrorHandler: HttpErrorHandler) {
        this.handleError = httpErrorHandler.createHandleError('ServersService');
    }

    getServers(): Observable<Server[]> {
        return this.http.get<Server[]>(this.serversUrl)
            .pipe(
                catchError(this.handleError('getServers', []))
            );
    }

    getServerStatus(server: Server) {
        return this.http.get<any>(this.serversUrl + `/${server.id}/status`)
            .pipe(
                catchError(this.handleError('getServerStatus', []))
            );

    }
}
