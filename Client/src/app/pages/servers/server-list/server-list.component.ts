import {Component, OnInit} from '@angular/core';
// App imports
import {Server} from '../server';
import {ServersService} from "../_services/servers-service";
import {AuthService} from "../../auth/_services/auth.service";
import {Router} from "@angular/router";
import {User} from "../../auth/user";

@Component({
    selector: 'app-server-list',
    templateUrl: './server-list.component.html',
    styleUrls: ['./server-list.component.scss']
})
export class ServerListComponent implements OnInit {
    // Using Bike Model class
    selectedServer: Server;
    servers: Server[];
    isLoading: Boolean = false;
    private user: User;
    private serverStatus: any;

    constructor(private services: ServersService, private router: Router,
                private authService: AuthService) {
    }

    ngOnInit() {
        if (!this.authService.isAuthenticated()) {
            this.router.navigate(['/login'])
        } else {
            this.authService.getUser().subscribe(
                user => this.user = user
            );
        }

        this.getServers();
    }

    getServers(): void {
        this.isLoading = true;
        this.services.getServers()
            .subscribe(
                response => this.handleResponse(response),
                error => this.handleError(error));
    }

    onChangeServer() {
        this.isLoading = true;

        this.services.getServerStatus(this.selectedServer).subscribe(
            response => this.handleStatusResponse(response),
            error => this.handleError(error));


    }

    protected handleStatusResponse(response: any) {
        this.isLoading = false;
        this.serverStatus = response;
        console.log(response);

    }

    protected handleResponse(response: Server[]) {
        this.isLoading = false,
            this.servers = response;
    }

    protected handleError(error: any) {
        this.isLoading = false,
            console.error(error);
    }

}
