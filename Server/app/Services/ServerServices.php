<?php

namespace App\Services;

use App\DatiCaricoServer;
use App\Server;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ServerServices
{

    const GREEN_STATUS = "GREEN";
    const ORANGE_STATUS = "ORANGE";
    const RED_STATUS = "RED";

    public function getStatus(Server $server,  $from = null,  $to = null)
    {
        if(!$from){
            $from  = new Carbon();
            $from->setDate(2018,8,1);
        }
        $from->startOfDay();

        if(!$to){
            $to  = new Carbon();
            $to->setDate(2018,8,1);
            $to->endOfMonth();
        }
        $to->endOfDay();

        $count = DatiCaricoServer::where('id_server', $server->id)
            ->select(\DB::raw("avg(carico_medio_cpu_percent) AS media_carico"))
            ->whereBetween('data', [
                $from,
                $to
            ])
            ->groupBy(\DB::raw('data, ora'))
            ->havingRaw('avg(carico_medio_cpu_percent) > 85')
            ->get()
            ->count();

        \Log::debug($count);

        return $count;

    }
}
