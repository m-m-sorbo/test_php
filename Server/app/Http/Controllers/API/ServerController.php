<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServersResource;
use App\Server;
use App\ServerAuth;
use App\Services\ServerServices;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Validator;

class ServerController extends Controller
{
    /**
     * Protect update and delete methods, only for authenticated users.
     *
     * @return Unauthorized
     */
    public function __construct()
    {
        $this->service = new ServerServices();
        $this->middleware('auth:api');


    }

    public function index(Request $request)
    {
        $user = $request->user();

        $server_ids = ServerAuth::where('user_id',$user->id)->pluck('server_id')->toArray();

        return Server::whereIn('id',$server_ids)->get();
    }

    public function show(Server $server)
    {
        return new ServersResource($server);
    }

    public function getStatus(Request $request,  $server_id)
    {
        $server = null;
        $user = $request->user();

        try {
            $server =  Server::findOrFail($server_id);
        } catch (ModelNotFoundException $e) {
            return response("Not found.", 404);
        }

        $serverAuth =  Server::join('server_auths', 'servers.id', 'server_auths.server_id')
            ->where('user_id', $user->id)->where('server_id',$server_id);
        if(!$serverAuth){
            return response("Unauthorized.", 401);

        }
        $status = $this->service->getStatus($server);

        return json_encode(compact('status'));
    }

}
