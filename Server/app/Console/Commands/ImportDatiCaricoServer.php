<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class ImportDatiCaricoServer extends Command
{

    private $default_path = 'dumpdb_dati_carico.sql';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:restore_dati_carico';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restores the database from a sql file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->default_path;

        \DB::unprepared(file_get_contents(storage_path($path)));

        $this->comment('Done!');
    }
}
