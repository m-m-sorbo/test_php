<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatiCaricoServer extends Model
{
    protected $table = 'dati_carico_server';
    protected $fillable = [
        'id_server',
        'data',
        'ora',
        'minuto',
        'carico_medio_cpu_percent',
    ];
}