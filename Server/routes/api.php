<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Register Routes
Route::post('login', 'API\AuthController@login');
Route::post('logout', 'API\AuthController@logout');

Route::get('servers/{id}/status', '\\' . \App\Http\Controllers\API\ServerController::class.  '@getStatus');
Route::get('servers', '\\' . \App\Http\Controllers\API\ServerController::class.  '@index');

Route::middleware('jwt.auth')->get('me', function (Request $request) {
    return auth()->user();
});

