<?php

use Illuminate\Database\Seeder;

class ServerAuthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dumpDate = [[1, 1], [2, 3], [3, 1], [5, 2], [2, 2], [1, 2], [1, 3], [5, 3], [4, 1]];

        foreach ($dumpDate as $dumpData) {
            DB::table('server_auths')->insert([
                'user_id'   => $dumpData[0],
                'server_id' => $dumpData[1],
            ]);
        }

    }
}
