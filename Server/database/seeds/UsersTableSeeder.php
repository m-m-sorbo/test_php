<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'admin',
            'password' => bcrypt('pippo')
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'giorgio',
            'password' => bcrypt('pluto')
        ]);
        DB::table('users')->insert([
            'id' => 3,
            'name' => 'pietro',
            'password' => bcrypt('paperino')
        ]);
        DB::table('users')->insert([
            'id' => 4,
            'name' => 'marco',
            'password' => bcrypt('paperone')
        ]);
        DB::table('users')->insert([
            'id' => 5,
            'name' => 'chiara',
            'password' => bcrypt('paperina')
        ]);




    }
}
