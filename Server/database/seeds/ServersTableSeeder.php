<?php

use Illuminate\Database\Seeder;

class ServersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servers')->delete();
        DB::table('servers')->insert([
            'id'   => 1,
            'name' => 'Server Milano',
            'ip'   => '62.149.195.202',
        ]);

        DB::table('servers')->insert([
            'id'      => 2,
            'name' => 'Server Roma',
            'ip'  => '62.149.193.246'
        ]);

        DB::table('servers')->insert([
            'id'      => 3,
            'name' => 'Server Bari',
            'ip'  => '62.149.194.31'
        ]);

    }
}